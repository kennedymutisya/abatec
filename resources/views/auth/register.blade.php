<!DOCTYPE html>
<!-- 
Template Name: Griffin - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Support: support@hencework.com

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>Abetec Signup</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="loader-pendulums"></div>
</div>
<!-- /Preloader -->

<!-- HK Wrapper -->
<div class="hk-wrapper">

    <!-- Main Content -->
    <div class="hk-pg-wrapper hk-auth-wrapper">
        <header class="d-flex justify-content-between align-items-center">
            <a class="d-flex auth-brand" href="#">
                <img class="brand-img" src="{{ asset('Abetech.png') }}" alt="brand"/>
            </a>
            <div class="btn-group btn-group-sm">
                <a href="#" class="btn btn-outline-secondary">Help</a>
                <a href="#" class="btn btn-outline-secondary">About Us</a>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-5 pa-0">
                    <div class="auth-cover-img overlay-wrap" style="background-image:url(web_developer.png);">
                        <div class="auth-cover-info py-xl-0 pt-100 pb-50">
                            <div class="auth-cover-content w-xxl-75 w-sm-90 w-100">
                                <h1 class="display-3 text-white mb-20">Welcome to Abetec</h1>
                                <p class="text-white">We will help you achieve your dreams.</p>
                                {{--                                <div class="play-wrap">--}}
                                {{--                                    <a class="play-btn" href="#"></a>--}}
                                {{--                                    <span>How it works ?</span>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                        <div class="bg-overlay bg-trans-dark-50"></div>
                    </div>
                </div>
                <div class="col-xl-7 pa-0">
                    <div class="auth-form-wrap py-xl-0 py-50">

                        <div class="auth-form w-xxl-55 w-xl-75 w-sm-90 w-100">
                            <form action="{{ route('register') }}" method="POST">
                                @csrf
                                <h1 class="display-4 mb-10">Sign up for free</h1>
                                <p class="mb-30">Create your account and start your journey.</p>
                                <div class="form-row">
                                    <div class="col-md-6 form-group">
                                        <input class="form-control {{ $errors->has('firstname') ? ' is-invalid' : '' }}"
                                               placeholder="First name" value="{{ old('firstname') }}" type="text"
                                               name="firstname">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input name="lastname" class="form-control {{ $errors->has('lastname') ? '
                                        is-invalid' : ''
                                         }}" placeholder="Last name" value="{{ old('lastname') }}" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" type="email"
                                           value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <input class="form-control {{ $errors->has('phonenumber') ? ' is-invalid' : '' }}"
                                    placeholder="Phone Number" type="text" value="{{ old('phonenumber') }}"
                                           name="phonenumber">
                                </div>
                                <div class="form-group">
                                    <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           placeholder="Password" type="password" value="{{ old('password') }}" name="password">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input id="password-confirm" type="password" class="form-control"
                                               placeholder="Password Confirmation"
                                               name="password_confirmation" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><span class="feather-icon"><i
                                                        data-feather="eye-off"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox mb-25">
                                    <input class="custom-control-input" id="same-address" type="checkbox" checked>
                                    <label class="custom-control-label font-14" for="same-address">I have read and agree
                                        to the <a href=""><u>term and conditions</u></a></label>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Register</button>
                                <div class="option-sep">or</div>

                                <p class="text-center">Already have an account? <a href="/login">Sign In</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content -->

</div>
<!-- /HK Wrapper -->

<!-- jQuery -->
<script src="vendors/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>

<!-- FeatherIcons JavaScript -->
<script src="dist/js/feather.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>

</body>

</html>
